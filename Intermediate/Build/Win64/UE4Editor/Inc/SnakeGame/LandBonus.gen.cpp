// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/LandBonus.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLandBonus() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_ALandBonus_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ALandBonus();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTimerHandle();
// End Cross Module References
	void ALandBonus::StaticRegisterNativesALandBonus()
	{
	}
	UClass* Z_Construct_UClass_ALandBonus_NoRegister()
	{
		return ALandBonus::StaticClass();
	}
	struct Z_Construct_UClass_ALandBonus_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VisibilityTimer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VisibilityTimer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandleVisibilityTimer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HandleVisibilityTimer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeathTimer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DeathTimer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ALandBonus_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALandBonus_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LandBonus.h" },
		{ "ModuleRelativePath", "LandBonus.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALandBonus_Statics::NewProp_VisibilityTimer_MetaData[] = {
		{ "ModuleRelativePath", "LandBonus.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ALandBonus_Statics::NewProp_VisibilityTimer = { "VisibilityTimer", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALandBonus, VisibilityTimer), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_ALandBonus_Statics::NewProp_VisibilityTimer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALandBonus_Statics::NewProp_VisibilityTimer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALandBonus_Statics::NewProp_HandleVisibilityTimer_MetaData[] = {
		{ "ModuleRelativePath", "LandBonus.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ALandBonus_Statics::NewProp_HandleVisibilityTimer = { "HandleVisibilityTimer", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALandBonus, HandleVisibilityTimer), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_ALandBonus_Statics::NewProp_HandleVisibilityTimer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALandBonus_Statics::NewProp_HandleVisibilityTimer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALandBonus_Statics::NewProp_DeathTimer_MetaData[] = {
		{ "ModuleRelativePath", "LandBonus.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ALandBonus_Statics::NewProp_DeathTimer = { "DeathTimer", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALandBonus, DeathTimer), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_ALandBonus_Statics::NewProp_DeathTimer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALandBonus_Statics::NewProp_DeathTimer_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ALandBonus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALandBonus_Statics::NewProp_VisibilityTimer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALandBonus_Statics::NewProp_HandleVisibilityTimer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALandBonus_Statics::NewProp_DeathTimer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ALandBonus_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ALandBonus>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ALandBonus_Statics::ClassParams = {
		&ALandBonus::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ALandBonus_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ALandBonus_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ALandBonus_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ALandBonus_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ALandBonus()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ALandBonus_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ALandBonus, 1832129471);
	template<> SNAKEGAME_API UClass* StaticClass<ALandBonus>()
	{
		return ALandBonus::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ALandBonus(Z_Construct_UClass_ALandBonus, &ALandBonus::StaticClass, TEXT("/Script/SnakeGame"), TEXT("ALandBonus"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ALandBonus);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
