// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_IndestructibleObject_generated_h
#error "IndestructibleObject.generated.h already included, missing '#pragma once' in IndestructibleObject.h"
#endif
#define SNAKEGAME_IndestructibleObject_generated_h

#define SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_RPC_WRAPPERS
#define SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAIndestructibleObject(); \
	friend struct Z_Construct_UClass_AIndestructibleObject_Statics; \
public: \
	DECLARE_CLASS(AIndestructibleObject, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AIndestructibleObject) \
	virtual UObject* _getUObject() const override { return const_cast<AIndestructibleObject*>(this); }


#define SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAIndestructibleObject(); \
	friend struct Z_Construct_UClass_AIndestructibleObject_Statics; \
public: \
	DECLARE_CLASS(AIndestructibleObject, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AIndestructibleObject) \
	virtual UObject* _getUObject() const override { return const_cast<AIndestructibleObject*>(this); }


#define SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AIndestructibleObject(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AIndestructibleObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AIndestructibleObject); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIndestructibleObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AIndestructibleObject(AIndestructibleObject&&); \
	NO_API AIndestructibleObject(const AIndestructibleObject&); \
public:


#define SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AIndestructibleObject(AIndestructibleObject&&); \
	NO_API AIndestructibleObject(const AIndestructibleObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AIndestructibleObject); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIndestructibleObject); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AIndestructibleObject)


#define SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_IndestructibleObject_h_12_PROLOG
#define SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_INCLASS \
	SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_IndestructibleObject_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class AIndestructibleObject>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_IndestructibleObject_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
