// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/BonusSpeed.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBonusSpeed() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_ABonusSpeed_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ABonusSpeed();
	SNAKEGAME_API UClass* Z_Construct_UClass_ALandBonus();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ABonusSpeed::execToggleBonusVisibility)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ToggleBonusVisibility_Implementation();
		P_NATIVE_END;
	}
	static FName NAME_ABonusSpeed_ToggleBonusVisibility = FName(TEXT("ToggleBonusVisibility"));
	void ABonusSpeed::ToggleBonusVisibility()
	{
		ProcessEvent(FindFunctionChecked(NAME_ABonusSpeed_ToggleBonusVisibility),NULL);
	}
	void ABonusSpeed::StaticRegisterNativesABonusSpeed()
	{
		UClass* Class = ABonusSpeed::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ToggleBonusVisibility", &ABonusSpeed::execToggleBonusVisibility },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ABonusSpeed_ToggleBonusVisibility_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABonusSpeed_ToggleBonusVisibility_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BonusSpeed.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABonusSpeed_ToggleBonusVisibility_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABonusSpeed, nullptr, "ToggleBonusVisibility", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABonusSpeed_ToggleBonusVisibility_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABonusSpeed_ToggleBonusVisibility_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABonusSpeed_ToggleBonusVisibility()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABonusSpeed_ToggleBonusVisibility_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ABonusSpeed_NoRegister()
	{
		return ABonusSpeed::StaticClass();
	}
	struct Z_Construct_UClass_ABonusSpeed_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABonusSpeed_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ALandBonus,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ABonusSpeed_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ABonusSpeed_ToggleBonusVisibility, "ToggleBonusVisibility" }, // 3625774917
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonusSpeed_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "BonusSpeed.h" },
		{ "ModuleRelativePath", "BonusSpeed.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ABonusSpeed_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ABonusSpeed, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABonusSpeed_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABonusSpeed>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABonusSpeed_Statics::ClassParams = {
		&ABonusSpeed::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABonusSpeed_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABonusSpeed_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABonusSpeed()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABonusSpeed_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABonusSpeed, 1449240604);
	template<> SNAKEGAME_API UClass* StaticClass<ABonusSpeed>()
	{
		return ABonusSpeed::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABonusSpeed(Z_Construct_UClass_ABonusSpeed, &ABonusSpeed::StaticClass, TEXT("/Script/SnakeGame"), TEXT("ABonusSpeed"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABonusSpeed);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
