// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ASnakeBase;
#ifdef SNAKEGAME_ProjectileBonus_generated_h
#error "ProjectileBonus.generated.h already included, missing '#pragma once' in ProjectileBonus.h"
#endif
#define SNAKEGAME_ProjectileBonus_generated_h

#define SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_RPC_WRAPPERS \
	virtual void ToggleBonusVisibility_Implementation(); \
 \
	DECLARE_FUNCTION(execGiveProjectile); \
	DECLARE_FUNCTION(execToggleBonusVisibility);


#define SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGiveProjectile); \
	DECLARE_FUNCTION(execToggleBonusVisibility);


#define SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_EVENT_PARMS
#define SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_CALLBACK_WRAPPERS
#define SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAProjectileBonus(); \
	friend struct Z_Construct_UClass_AProjectileBonus_Statics; \
public: \
	DECLARE_CLASS(AProjectileBonus, ALandBonus, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AProjectileBonus) \
	virtual UObject* _getUObject() const override { return const_cast<AProjectileBonus*>(this); }


#define SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAProjectileBonus(); \
	friend struct Z_Construct_UClass_AProjectileBonus_Statics; \
public: \
	DECLARE_CLASS(AProjectileBonus, ALandBonus, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AProjectileBonus) \
	virtual UObject* _getUObject() const override { return const_cast<AProjectileBonus*>(this); }


#define SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjectileBonus(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjectileBonus) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectileBonus); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectileBonus); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectileBonus(AProjectileBonus&&); \
	NO_API AProjectileBonus(const AProjectileBonus&); \
public:


#define SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjectileBonus() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectileBonus(AProjectileBonus&&); \
	NO_API AProjectileBonus(const AProjectileBonus&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectileBonus); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectileBonus); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AProjectileBonus)


#define SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_ProjectileBonus_h_15_PROLOG \
	SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_EVENT_PARMS


#define SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_CALLBACK_WRAPPERS \
	SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_INCLASS \
	SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_CALLBACK_WRAPPERS \
	SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_ProjectileBonus_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class AProjectileBonus>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_ProjectileBonus_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
