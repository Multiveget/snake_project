// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_LandBonus_generated_h
#error "LandBonus.generated.h already included, missing '#pragma once' in LandBonus.h"
#endif
#define SNAKEGAME_LandBonus_generated_h

#define SnakeGame_Source_SnakeGame_LandBonus_h_12_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_LandBonus_h_12_RPC_WRAPPERS
#define SnakeGame_Source_SnakeGame_LandBonus_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame_Source_SnakeGame_LandBonus_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALandBonus(); \
	friend struct Z_Construct_UClass_ALandBonus_Statics; \
public: \
	DECLARE_CLASS(ALandBonus, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ALandBonus)


#define SnakeGame_Source_SnakeGame_LandBonus_h_12_INCLASS \
private: \
	static void StaticRegisterNativesALandBonus(); \
	friend struct Z_Construct_UClass_ALandBonus_Statics; \
public: \
	DECLARE_CLASS(ALandBonus, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ALandBonus)


#define SnakeGame_Source_SnakeGame_LandBonus_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALandBonus(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALandBonus) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALandBonus); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALandBonus); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALandBonus(ALandBonus&&); \
	NO_API ALandBonus(const ALandBonus&); \
public:


#define SnakeGame_Source_SnakeGame_LandBonus_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALandBonus(ALandBonus&&); \
	NO_API ALandBonus(const ALandBonus&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALandBonus); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALandBonus); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ALandBonus)


#define SnakeGame_Source_SnakeGame_LandBonus_h_12_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_LandBonus_h_9_PROLOG
#define SnakeGame_Source_SnakeGame_LandBonus_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_LandBonus_h_12_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_LandBonus_h_12_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_LandBonus_h_12_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_LandBonus_h_12_INCLASS \
	SnakeGame_Source_SnakeGame_LandBonus_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_LandBonus_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_LandBonus_h_12_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_LandBonus_h_12_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_LandBonus_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_LandBonus_h_12_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_LandBonus_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ALandBonus>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_LandBonus_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
