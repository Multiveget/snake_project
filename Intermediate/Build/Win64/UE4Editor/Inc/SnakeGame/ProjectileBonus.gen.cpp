// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/ProjectileBonus.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeProjectileBonus() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_AProjectileBonus_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_AProjectileBonus();
	SNAKEGAME_API UClass* Z_Construct_UClass_ALandBonus();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	SNAKEGAME_API UClass* Z_Construct_UClass_ASnakeBase_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AProjectileBonus::execGiveProjectile)
	{
		P_GET_OBJECT(ASnakeBase,Z_Param_SnakeActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GiveProjectile(Z_Param_SnakeActor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AProjectileBonus::execToggleBonusVisibility)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ToggleBonusVisibility_Implementation();
		P_NATIVE_END;
	}
	static FName NAME_AProjectileBonus_ToggleBonusVisibility = FName(TEXT("ToggleBonusVisibility"));
	void AProjectileBonus::ToggleBonusVisibility()
	{
		ProcessEvent(FindFunctionChecked(NAME_AProjectileBonus_ToggleBonusVisibility),NULL);
	}
	void AProjectileBonus::StaticRegisterNativesAProjectileBonus()
	{
		UClass* Class = AProjectileBonus::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GiveProjectile", &AProjectileBonus::execGiveProjectile },
			{ "ToggleBonusVisibility", &AProjectileBonus::execToggleBonusVisibility },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AProjectileBonus_GiveProjectile_Statics
	{
		struct ProjectileBonus_eventGiveProjectile_Parms
		{
			ASnakeBase* SnakeActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SnakeActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AProjectileBonus_GiveProjectile_Statics::NewProp_SnakeActor = { "SnakeActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ProjectileBonus_eventGiveProjectile_Parms, SnakeActor), Z_Construct_UClass_ASnakeBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AProjectileBonus_GiveProjectile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AProjectileBonus_GiveProjectile_Statics::NewProp_SnakeActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AProjectileBonus_GiveProjectile_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ProjectileBonus.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AProjectileBonus_GiveProjectile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AProjectileBonus, nullptr, "GiveProjectile", nullptr, nullptr, sizeof(ProjectileBonus_eventGiveProjectile_Parms), Z_Construct_UFunction_AProjectileBonus_GiveProjectile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AProjectileBonus_GiveProjectile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AProjectileBonus_GiveProjectile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AProjectileBonus_GiveProjectile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AProjectileBonus_GiveProjectile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AProjectileBonus_GiveProjectile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AProjectileBonus_ToggleBonusVisibility_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AProjectileBonus_ToggleBonusVisibility_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ProjectileBonus.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AProjectileBonus_ToggleBonusVisibility_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AProjectileBonus, nullptr, "ToggleBonusVisibility", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AProjectileBonus_ToggleBonusVisibility_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AProjectileBonus_ToggleBonusVisibility_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AProjectileBonus_ToggleBonusVisibility()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AProjectileBonus_ToggleBonusVisibility_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AProjectileBonus_NoRegister()
	{
		return AProjectileBonus::StaticClass();
	}
	struct Z_Construct_UClass_AProjectileBonus_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AProjectileBonus_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ALandBonus,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AProjectileBonus_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AProjectileBonus_GiveProjectile, "GiveProjectile" }, // 3542247273
		{ &Z_Construct_UFunction_AProjectileBonus_ToggleBonusVisibility, "ToggleBonusVisibility" }, // 1345299761
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AProjectileBonus_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "ProjectileBonus.h" },
		{ "ModuleRelativePath", "ProjectileBonus.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AProjectileBonus_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(AProjectileBonus, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AProjectileBonus_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AProjectileBonus>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AProjectileBonus_Statics::ClassParams = {
		&AProjectileBonus::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AProjectileBonus_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AProjectileBonus_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AProjectileBonus()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AProjectileBonus_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AProjectileBonus, 3994218925);
	template<> SNAKEGAME_API UClass* StaticClass<AProjectileBonus>()
	{
		return AProjectileBonus::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AProjectileBonus(Z_Construct_UClass_AProjectileBonus, &AProjectileBonus::StaticClass, TEXT("/Script/SnakeGame"), TEXT("AProjectileBonus"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AProjectileBonus);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
