// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LandBonus.generated.h"

UCLASS()
class SNAKEGAME_API ALandBonus : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALandBonus();
	UPROPERTY()
		FTimerHandle DeathTimer;
	UPROPERTY()
		FTimerHandle HandleVisibilityTimer;
	UPROPERTY()
		FTimerHandle VisibilityTimer;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void OnDeath();
	virtual void HandleVisibility();
	virtual void ToggleBonusVisibility();
};
