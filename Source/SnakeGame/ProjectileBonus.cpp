// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileBonus.h"
#include "SnakeBase.h"
#include "Projectile.h"

void AProjectileBonus::ToggleBonusVisibility_Implementation()
{
}

void AProjectileBonus::GiveProjectile(ASnakeBase* SnakeActor)
{
	SnakeActor->AmmountOfProjectiles += 1;
}

void AProjectileBonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		GiveProjectile(Snake);
		this->Destroy();
	}
}
