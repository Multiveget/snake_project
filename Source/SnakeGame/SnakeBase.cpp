// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Math/UnrealMathUtility.h"
#include "Food.h"
#include "ProjectileBonus.h"
#include "IndestructibleObject.h"
#include "BonusSpeed.h"
#include "Projectile.h"


// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirecton = EMovementDirection::DOWN;
	AmmountOfProjectiles = 0;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(1);
	AddIndestructElement(2);
	for (int i = 0; i < IndestructibleElements.Num(); i++)
	{
		FVector ElemLocation = IndestructibleElements[i]->GetActorLocation();
		IndestrucLocations.Add(ElemLocation);
	}
	SpawnFood();
	GetWorld()->GetTimerManager().SetTimer(SpeedBonusSpawnHandler, this, &ASnakeBase::SpawnSpeedBonus, 5, true);
	GetWorld()->GetTimerManager().SetTimer(ProjectileBonusSpawnHandler, this, &ASnakeBase::SpawnProjectileBonus, 5, true);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation;
		//��������� ����� ������ ���������� ��������
		if (SnakeElements.Num() == 0)
		{
			NewLocation={SnakeElements.Num() * ElementSize, 0, 0 };
		}
		else
		{
			NewLocation = LastElemPrevLocation;
		}
		// ����������� ����������
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex=SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	FVector HeadLocation = SnakeElements[0]->GetActorLocation();
		switch (LastMoveDirecton)
		{
		case EMovementDirection::UP:
			MovementVector.X += ElementSize;
			if (HeadLocation.X > (500 - ElementSize))
			{
				FVector NewHeadLocation(-500, HeadLocation.Y, HeadLocation.Z);
				SnakeElements[0]->SetActorLocation(NewHeadLocation);
			}
			break;
		case EMovementDirection::DOWN:
			MovementVector.X -= ElementSize;
			if (HeadLocation.X < (-500 + ElementSize))
			{
				FVector NewHeadLocation(500, HeadLocation.Y, HeadLocation.Z);
				SnakeElements[0]->SetActorLocation(NewHeadLocation);
			}
			break;
		case EMovementDirection::LEFT:
			MovementVector.Y += ElementSize;
			if (HeadLocation.Y > (500 - ElementSize))
			{
				FVector NewHeadLocation(HeadLocation.X, -500, HeadLocation.Z);
				SnakeElements[0]->SetActorLocation(NewHeadLocation);
			}
			break;
		case EMovementDirection::RIGHT:
			MovementVector.Y -= ElementSize;
			if (HeadLocation.Y < (-500 + ElementSize))
			{
				FVector NewHeadLocation(HeadLocation.X, 500, HeadLocation.Z);
				SnakeElements[0]->SetActorLocation(NewHeadLocation);
			}
			break;
		}
		SnakeElements[0]->ToggleCollision();
		for (int i = SnakeElements.Num() - 1; i > 0; i--)
		{
			auto CurrentElement = SnakeElements[i];
			auto PrevElement = SnakeElements[i - 1];
			FVector PrevLocation = PrevElement->GetActorLocation();
			CurrentElement->SetActorLocation(PrevLocation);
		}
		//������ ���������� ��������
		LastElemPrevLocation = SnakeElements.Last()->GetActorLocation();
		SnakeElements[0]->AddActorWorldOffset(MovementVector);
		SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}


void ASnakeBase::SpawnFood()
{
	bool bEverythingIsNotOk = true; //�����������, ������ �� Location ��� � Location ������ � �������������� ������ 
	while (bEverythingIsNotOk)
	{
		float x = FMath::RandRange(-500, 500);
		float y = FMath::RandRange(-500, 500);
		FVector TempLocation(x, y, -10);
		FVector HeadLocation=SnakeElements[0]->GetActorLocation();
		if (TempLocation.X != HeadLocation.X || TempLocation.Y != HeadLocation.Y)
		{
			bEverythingIsNotOk = false;
		}
		if (bEverythingIsNotOk == false)
		{
			for (int i = 0; i < IndestructibleElements.Num(); i++)
			{
				if (TempLocation.X == IndestrucLocations[i].X && TempLocation.Y == IndestrucLocations[i].Y)
				{
					bEverythingIsNotOk = true;
					break;
				}
			}
		}
		if (bEverythingIsNotOk == false)
		{
			FTransform FoodTransform(TempLocation);
			GetWorld()->SpawnActor<AFood>(FoodClass, FoodTransform);
		}
	}
}

void ASnakeBase::AddIndestructElement(int IndestructElements)
{
	for (int i = 0; i < IndestructElements; i++)
	{
		bool bIsHead = true;
		while (bIsHead)
		{
			float x = FMath::RandRange(-500, 500);
			float y = FMath::RandRange(-500, 500);
			FVector TempLocation(x, y, -10);
			FVector HeadLocation = SnakeElements[0]->GetActorLocation();
			float XRange = FMath::Abs(TempLocation.X - HeadLocation.X);
			float YRange= FMath::Abs(TempLocation.Y - HeadLocation.Y);
			if (XRange>=50 && YRange>=50)
			{
				bIsHead = false;
				FTransform IndestructTransform(TempLocation);
				auto NewIndestruct= GetWorld()->SpawnActor<AIndestructibleObject>(IndestructibleClass, IndestructTransform);
				IndestructibleElements.Add(NewIndestruct);
			}
		}
	}
}

void ASnakeBase::OnBonusSpeedOverlap()
{
	SetActorTickInterval(MovementSpeed);
	GetWorld()->GetTimerManager().ClearTimer(SpeedTimerHandle);
}

void ASnakeBase::SpawnSpeedBonus()
{
	int random_value = FMath::RandRange(1, 10);
	if (random_value > 3)
	{
		float x = FMath::RandRange(-500, 500);
		float y = FMath::RandRange(-500, 500);
		FVector TempLocation(x, y, -10);
		FTransform BonusTransform(TempLocation);
		GetWorld()->SpawnActor<ABonusSpeed>(SpeedBonusClass, BonusTransform);
	}
}

void ASnakeBase::SpawnProjectileBonus()
{
	int random_value = FMath::RandRange(1, 10);
	if (random_value > 3)
	{
		float x = FMath::RandRange(-500, 500);
		float y = FMath::RandRange(-500, 500);
		FVector TempLocation(x, y, -10);
		FTransform BonusTransform(TempLocation);
		GetWorld()->SpawnActor<AProjectileBonus>(ProjectileBonusClass, BonusTransform);
	}
}

void ASnakeBase::Shoot()
{
	if (AmmountOfProjectiles != 0)
	{
		AmmountOfProjectiles -= 1;
		FVector SpawnVector = SnakeElements[0]->GetActorLocation();
		switch (LastMoveDirecton)
		{
		case(EMovementDirection::UP):
			SpawnVector.X += ElementSize;
			break;
		case(EMovementDirection::DOWN):
			SpawnVector.X -= ElementSize;
			break;
		case(EMovementDirection::LEFT):
			SpawnVector.Y += ElementSize;
			break;
		case(EMovementDirection::RIGHT):
			SpawnVector.Y -= ElementSize;
		}
		FTransform SpawnTransform(SpawnVector);
		auto SpawnedProjectile=GetWorld()->SpawnActor<AProjectile>(ProjectileClass, SpawnTransform);
		SpawnedProjectile->ParentSnake = this;
		SpawnedProjectile->SetDirection();
	}
}

