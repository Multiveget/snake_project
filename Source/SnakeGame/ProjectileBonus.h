// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LandBonus.h"
#include "Interactable.h"
#include "ProjectileBonus.generated.h"

class ASnakeBase;

/**
 * 
 */
UCLASS()
class SNAKEGAME_API AProjectileBonus : public ALandBonus, public IInteractable
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintNativeEvent)
		void ToggleBonusVisibility() override;
		void ToggleBonusVisibility_Implementation();
	UFUNCTION()
		void GiveProjectile(ASnakeBase* SnakeActor);
	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
