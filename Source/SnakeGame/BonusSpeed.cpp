// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusSpeed.h"
#include "SnakeBase.h"

void ABonusSpeed::ToggleBonusVisibility_Implementation()
{
}

void ABonusSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		float CurrentSpeed = Snake->MovementSpeed;
		float NewSpeed = CurrentSpeed / 2;
		Snake->SetActorTickInterval(NewSpeed);
		GetWorld()->GetTimerManager().SetTimer(Snake->SpeedTimerHandle, Snake, &ASnakeBase::OnBonusSpeedOverlap, 5.0f, false);
		this->Destroy();
	}
}
