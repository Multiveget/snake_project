// Fill out your copyright notice in the Description page of Project Settings.


#include "LandBonus.h"

// Sets default values
ALandBonus::ALandBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ALandBonus::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(DeathTimer, this, &ALandBonus::OnDeath, 5, false);
	GetWorld()->GetTimerManager().SetTimer(HandleVisibilityTimer, this, &ALandBonus::HandleVisibility, 3, false);
}

// Called every frame
void ALandBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALandBonus::OnDeath()
{
	this->Destroy();
	GetWorld()->GetTimerManager().ClearTimer(DeathTimer);
}

void ALandBonus::HandleVisibility()
{
	GetWorld()->GetTimerManager().SetTimer(VisibilityTimer, this, &ALandBonus::ToggleBonusVisibility, 0.25f, true);
	GetWorld()->GetTimerManager().ClearTimer(HandleVisibilityTimer);
}

void ALandBonus::ToggleBonusVisibility()
{
}

