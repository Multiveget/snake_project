// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "IndestructibleObject.generated.h"

class AProjectile;

UCLASS()
class SNAKEGAME_API AIndestructibleObject : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AIndestructibleObject();
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int health;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	void InteractProjectiles(AProjectile* OverlappedProjectile);
};
